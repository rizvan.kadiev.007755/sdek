from services.cookies import Cookies
import json


class CityID:
    @classmethod
    def get_sender_city_id(cls, sender_city) -> str:
        session = Cookies.get_cookies()
        url = f"https://www.cdek.ru/api-lkfl/cities/autocomplete?str={sender_city}&page=1&perPage=10"
        response = session.get(url)
        response_text = response.text
        data = json.loads(response_text)
        data_list = data.get('data', [])
        first_uuid = data_list[0].get('uuid', None)
        print(f"Отправитель {sender_city} - sender_uuid {first_uuid}")
        return first_uuid

    @classmethod
    def get_receiver_city_id(cls, receiver_city) -> str:
        session = Cookies.get_cookies()
        url = f"https://www.cdek.ru/api-lkfl/cities/autocomplete?str={receiver_city}&page=1&perPage=10"
        response = session.get(url)
        response_text = response.text
        data = json.loads(response_text)
        data_list = data.get('data', [])
        first_uuid = data_list[0].get('uuid', None)
        print(f"Получатель {receiver_city} - receiver_uuid {first_uuid}")
        return first_uuid
