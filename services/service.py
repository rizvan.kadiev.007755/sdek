from services.cookies import Cookies


class Service:
    service_id = ""

    @classmethod
    def post_service_data(cls, sender_id, receiver_id, weight_value, size_data):
        session = Cookies.get_cookies()
        url = "https://www.cdek.ru/api-lkfl/estimateV2"
        data = {
            "currencyMark": "RUB",
            "packages": [
                {"height": size_data["height"],
                 "length": size_data["length"],
                 "width": size_data["width"],
                 "weight": weight_value}
            ],
            "payerType": "sender",
            "receiverCityId": receiver_id,
            "senderCityId": sender_id
        }
        response = session.post(url, json=data)
        data = response.json()
        return data

    @classmethod
    def get_data_service_id(cls, data):
        for item in data['data']:
            if item['description'] == "Можно вызвать курьера":
                for tariff in item['tariffs']:
                    if tariff['mode'] == 'HOME-HOME':
                        cls.service_id = tariff['serviceId']
        print("Service ID: ", cls.service_id)
        return cls.service_id

    @staticmethod
    def get_date_from_delivery_days(data):
        for item in data['data']:
            if item['description'] == "Можно вызвать курьера":
                date_from = item["minDays"]
                print(f"Доставка от {date_from} дня")
                return date_from

    @staticmethod
    def get_date_until_delivery_days(data):
        for item in data['data']:
            if item['description'] == "Можно вызвать курьера":
                date_until = item["maxDays"]
                print(f"Доставка до {date_until} дней")
                return date_until
