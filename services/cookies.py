import requests
from selenium import webdriver


class Cookies:
    @classmethod
    def get_cookies(cls):
        firefox_profile_path = '/Users/rizvan/Library/Application Support/Firefox/Profiles/92eqyfmx.default-release'
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.add_argument('--headless')
        firefox_options.set_preference("browser.startup.homepage", "https://www.cdek.ru/ru/cabinet/calculate")
        firefox_options.set_preference("browser.startup.page", 1)
        firefox_options.set_preference("browser.startup.homepage_override.mstone", "ignore")
        firefox_options.set_preference("browser.startup.page_override.delay", 0)
        firefox_options.set_preference("browser.shell.checkDefaultBrowser", False)
        firefox_options.set_preference("browser.preferences.instantApply", True)
        firefox_options.profile = firefox_profile_path
        driver = webdriver.Firefox(options=firefox_options)
        browser_cookies = driver.get_cookies()
        session = requests.Session()
        for c in browser_cookies:
            session.cookies.set(c['name'], c['value'])
        driver.refresh()
        driver.quit()
        return session
