class Route:
    def __init__(self, sender_city, receiver_city, weight_05, weight_1, weight_2, weight_3, weight_4, weight_5,
                 weight_20, date_from, date_until):
        self.sender_city = sender_city
        self.receiver_city = receiver_city
        self.weight_05 = weight_05
        self.weight_1 = weight_1
        self.weight_2 = weight_2
        self.weight_3 = weight_3
        self.weight_4 = weight_4
        self.weight_5 = weight_5
        self.weight_20 = weight_20
        self.date_from = date_from
        self.date_until = date_until

    def __str__(self):
        return (f"Отправитель: {self.sender_city}, Получатель: {self.receiver_city}\n"
                f"0.5кг: {self.weight_05}\n"
                f"1кг: {self.weight_1}\n"
                f"2кг: {self.weight_2}\n"
                f"3кг: {self.weight_3}\n"
                f"4кг: {self.weight_4}\n"
                f"5кг: {self.weight_5}\n"
                f"20кг: {self.weight_20}\n"
                f"От: {self.date_from}\n"
                f"До: {self.date_until}")

    def update_price(self, weight, price):
        weight_attribute = f"weight_{str(weight).replace('.', '')}"
        if hasattr(self, weight_attribute):
            setattr(self, weight_attribute, price)
        else:
            print(f"Attribute {weight_attribute} not found in Route class.")

    def update_delivery_days(self, date_from, date_until):
        if date_from is not None:
            setattr(self, 'date_from', date_from)
        if date_until is not None:
            setattr(self, 'date_until', date_until)

    @classmethod
    def from_excel_row(cls, row):
        return cls(*row[1:])
