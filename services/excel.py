import openpyxl
from services.route import Route


class File:
    @staticmethod
    def load_routes_from_excel(file_path):
        workbook = openpyxl.load_workbook(file_path)
        sheet = workbook.active
        routes = []
        for row in sheet.iter_rows(min_row=4, values_only=True):
            route = Route.from_excel_row(row)
            routes.append(route)
        return routes

    @staticmethod
    def update_file(file_path, routes):
        workbook = openpyxl.load_workbook(file_path)
        sheet = workbook.active

        for i, route in enumerate(routes):
            row_number = i + 4
            sheet.cell(row=row_number, column=4, value=route.weight_05)
            sheet.cell(row=row_number, column=5, value=route.weight_1)
            sheet.cell(row=row_number, column=6, value=route.weight_2)
            sheet.cell(row=row_number, column=7, value=route.weight_3)
            sheet.cell(row=row_number, column=8, value=route.weight_4)
            sheet.cell(row=row_number, column=9, value=route.weight_5)
            sheet.cell(row=row_number, column=10, value=route.weight_20)
            sheet.cell(row=row_number, column=11, value=route.date_from)
            sheet.cell(row=row_number, column=12, value=route.date_until)

        workbook.save("price_update.xlsx")
