from services.cookies import Cookies


class TotalCost:

    @classmethod
    def parse(cls, sender_id, receiver_id, service_id, weight_value, size_data):
        session = Cookies.get_cookies()
        url = "https://www.cdek.ru/api-lkfl/getTariffInfo"
        data = {
            "withoutAdditionalServices": 0,
            "mode": "HOME-HOME",
            "payerType": "sender",
            "currencyMark": "RUB",
            "senderCityId": sender_id,
            "receiverCityId": receiver_id,
            "packages": [
                {
                    "height": size_data["height"],
                    "length": size_data["length"],
                    "width": size_data["width"],
                    "weight": weight_value}
            ],
            "serviceId": str(service_id),
            "additionalServices": []
            # в список можно передать упаковку в зависимости от размеров коробки и веса,
            # и также страховку
        }
        response = session.post(url, json=data)
        data = response.json()
        totalCost = data['data']['totalCost']
        print("Итоговая сумма: ", totalCost)
        return totalCost
