import unittest
from unittest.mock import Mock, patch
from services.service import Service
from services.cookies import Cookies


class TestService(unittest.TestCase):
    @patch('services.cookies.Cookies.get_cookies')
    def test_post_service_data(self, mock_get_cookies):
        mock_session = Mock()
        mock_get_cookies.return_value = mock_session
        mock_response = Mock()
        mock_response.json.return_value = {"data": [{"description": "Можно вызвать курьера"}]}
        mock_session.post.return_value = mock_response
        sender_id = "sender_id"
        receiver_id = "receiver_id"
        weight_value = 10
        size_data = {"height": 10, "length": 20, "width": 30}
        result = Service.post_service_data(sender_id, receiver_id, weight_value, size_data)
        self.assertEqual(result, {"data": [{"description": "Можно вызвать курьера"}]})
        mock_get_cookies.assert_called_once()
        mock_session.post.assert_called_once_with(
            "https://www.cdek.ru/api-lkfl/estimateV2",
            json={
                "currencyMark": "RUB",
                "packages": [{"height": 10, "length": 20, "width": 30, "weight": 10}],
                "payerType": "sender",
                "receiverCityId": "receiver_id",
                "senderCityId": "sender_id",
            },
        )

    def test_get_data_service_id(self):
        data = {
            "data": [
                {
                    "description": "Можно вызвать курьера",
                    "tariffs": [{"mode": "HOME-HOME", "serviceId": "123"}],
                }
            ]
        }
        result = Service.get_data_service_id(data)

        self.assertEqual(result, "123")

    def test_get_date_from_delivery_days(self):
        data = {"data": [{"description": "Можно вызвать курьера", "minDays": 2}]}
        result = Service.get_date_from_delivery_days(data)
        self.assertEqual(result, 2)

    def test_get_date_until_delivery_days(self):
        data = {"data": [{"description": "Можно вызвать курьера", "maxDays": 5}]}
        result = Service.get_date_until_delivery_days(data)
        self.assertEqual(result, 5)


if __name__ == '__main__':
    unittest.main()
