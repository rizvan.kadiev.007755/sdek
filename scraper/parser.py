from services.service import Service
from services.route import Route
from services.excel import File
from services.cost import TotalCost
from services.cookies import Cookies
from services.city import CityID


class Parser:
    routes = File.load_routes_from_excel(file_path="price.xlsx")
    size_mapping = {
        0.5: {"length": 17, "width": 12, "height": 9},
        1: {"length": 42, "width": 36, "height": 5},
        2: {"length": 42, "width": 36, "height": 5},
        3: {"length": 33, "width": 25, "height": 15},
        4: {"length": 33, "width": 25, "height": 15},
        5: {"length": 33, "width": 25, "height": 15},
        20: {"length": 60, "width": 60, "height": 30}

    }

    @classmethod
    def parse(cls):
        for route in cls.routes:
            min_days = []
            max_days = []
            print(route)
            receiver_id = CityID.get_receiver_city_id(route.receiver_city)
            sender_id = CityID.get_sender_city_id(route.sender_city)
            for weight, size_data in cls.size_mapping.items():
                data = Service.post_service_data(sender_id, receiver_id, weight, size_data)
                service_id = Service.get_data_service_id(data)
                date_from_delivery_days = Service.get_date_from_delivery_days(data)
                date_until_delivery_days = Service.get_date_until_delivery_days(data)
                min_days.append(date_from_delivery_days)
                max_days.append(date_until_delivery_days)
                total_cost = TotalCost.parse(sender_id, receiver_id, service_id, weight, size_data)
                route.update_price(weight, total_cost)
            route.update_delivery_days(min(min_days), max(max_days))
            print(route)
            File.update_file(file_path="price.xlsx", routes=cls.routes)
